<?php
// Drupal themes designed by DrupalThemeBank.com.
// Created Feb 13, 2009, Last Updated: Feb 16, 2009
?>
  <div class="box">
    <?php if ($title) { ?><h2 class="title"><?php print $title; ?></h2><?php } ?>
    <div class="content"><?php print $content; ?></div>
 </div>

